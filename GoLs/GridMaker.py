from sklearn import neighbors


def Moore(X,Y):
    neighbors=[]
    Edges=[]
    for x in range(1,X+1):
        Edges.append((x,1))
        Edges.append((x,Y))
    for y in range(2,Y):
        Edges.append((1,y))
        Edges.append((X,y))
    for x in range(1,X+1):
        for y in range(1,Y+1):
            if(x<X):
                neighbors.append((x,y,x+1,y))
            if(y<Y):
                neighbors.append((x,y,x,y+1))

            if(x>1):
                neighbors.append((x,y,x-1,y))
            if(y>1):
                neighbors.append((x,y,x,y-1))
            if(x<X and y<Y):
                neighbors.append((x,y,x+1,y+1))
            if(x>1 and y>1):
                neighbors.append((x,y,x-1,y-1))
            if(x<X and y>1):
                neighbors.append((x,y,x+1,y-1))
            if(y<Y and x>1):
                neighbors.append((x,y,x-1,y+1))

            # if(x<X-1):
            #     neighbors.append((x,y,x+2,y))
            # if(y<Y-1):
            #     neighbors.append((x,y,x,y+2))
            # if(x<X-1 and y<Y-1):
            #     neighbors.append((x,y,x+1,y+2))
            #     neighbors.append((x,y,x+2,y+1))
            #     neighbors.append((x,y,x+2,y+2))
            # if(x>2):
            #     neighbors.append((x,y,x-2,y))
            # if(y>2):
            #     neighbors.append((x,y,x,y-2))
            # if(x>2 and y>2):
            #     neighbors.append((x,y,x-1,y-2))
            #     neighbors.append((x,y,x-2,y-1))
            #     neighbors.append((x,y,x-2,y-2))
            # if(x<X-1 and y>2):
            #     neighbors.append((x,y,x+2,y-1))
            #     neighbors.append((x,y,x+1,y-2))
            #     neighbors.append((x,y,x+2,y-2))
            # if(y<Y-1 and x>2):
            #     neighbors.append((x,y,x-1,y+2))
            #     neighbors.append((x,y,x-2,y+1))
            #     neighbors.append((x,y,x-2,y+2))
    return Edges,neighbors
def Neumann(X,Y):
    neighbors=[]
    Edges=[]
    for x in range(1,X+1):
        Edges.append((x,1))
        Edges.append((x,Y))
    for y in range(2,Y):
        Edges.append((1,y))
        Edges.append((X,y))
    for x in range(1,X+1):
        for y in range(1,Y+1):
            if(x<X):
                neighbors.append((x,y,x+1,y))
            if(y<Y):
                neighbors.append((x,y,x,y+1))

            if(x>1):
                neighbors.append((x,y,x-1,y))
            if(y>1):
                neighbors.append((x,y,x,y-1))
            
    return Edges,neighbors


def DoubleMoore(X,Y):
    neighbors=[]
    Edges=[]
    for x in range(1,X+1):
        Edges.append((x,1))
        Edges.append((x,Y))
    for y in range(2,Y):
        Edges.append((1,y))
        Edges.append((X,y))
    for x in range(1,X+1):
        for y in range(1,Y+1):
            if(x<X-1):
                neighbors.append((x,y,x+2,y))
            if(y<Y-1):
                neighbors.append((x,y,x,y+2))
            if(x<X-1 and y<Y-1):
                neighbors.append((x,y,x+1,y+2))
                neighbors.append((x,y,x+2,y+1))
                neighbors.append((x,y,x+2,y+2))
            if(x>2):
                neighbors.append((x,y,x-2,y))
            if(y>2):
                neighbors.append((x,y,x,y-2))
            if(x>2 and y>2):
                neighbors.append((x,y,x-1,y-2))
                neighbors.append((x,y,x-2,y-1))
                neighbors.append((x,y,x-2,y-2))
            if(x<X-1 and y>2):
                neighbors.append((x,y,x+2,y-1))
                neighbors.append((x,y,x+1,y-2))
                neighbors.append((x,y,x+2,y-2))
            if(y<Y-1 and x>2):
                neighbors.append((x,y,x-1,y+2))
                neighbors.append((x,y,x-2,y+1))
                neighbors.append((x,y,x-2,y+2))
            return Edges,neighbors


