from os import replace
from pyidp3.typedIDP import IDP
import ast
from math import ceil
from GridMaker import Neumann,Moore,DoubleMoore
def Gens(size,liv,genr,movH,movV):
    genr+=1
    realsize=size+2
    cells=size*size
    GRID= Moore(realsize,realsize)
    EdgeList=GRID[0]
    NbList=GRID[1]    
    
# Declaration of our idp knowledge base ---------------------------------------------
    idp = IDP("/home/jorne/Documents/idp/idp3-3.7.1-Linux/usr/local/bin/idp")
    idp.nbmodels = 1

    sliv=str(liv-1)

    idp.Type("Number", (0, 1000))

    idp.Type("Row", (1,realsize))
    idp.Type("Column", (1,realsize))


    idp.Type("NbA", list(range(0,23)))
    idp.Type("t", (0,genr-1))
    idp.Constant("dead: Number")

    idp.Predicate("Nb(Row,Column,Row,Column)",NbList)
    idp.Predicate("EdgeCell(Row,Column)",EdgeList)
    idp.Predicate("Young(t,Row,Column)")
    idp.Predicate("Old(t,Row,Column)")

    idp.Predicate("Seed(Row,Column)")
    idp.Function("NbAlive(t,Row, Column): NbA")

    #The edgecell massacre
    idp.Constraint("!r1 c1 g1:EdgeCell(r1,c1)=>~Young(g1,r1,c1)&~Old(g1,r1,c1).", True)

    #How many neigbours are alive?
    idp.Constraint("!r1 c1 g1: NbAlive(g1,r1,c1)=#{r c:(Young(g1,r,c)|Old(g1,r,c))&Nb(r1,c1,r,c)}.", True)
    idp.Constraint("#{r1[Row] c1[Column] : Seed(r1,c1) }>"+sliv, True) 

    
    #Rules of lives -----------------------------------------------------------------------------------
    idp.Define("!r1,c1:Young(0,r1,c1)<-Seed(r1,c1). \n !r1,c1 t:Young(t,r1,c1)<-NbAlive(t-1,r1,c1)=3&~Old(t-1,r1,c1) & ~Young(t-1,r1,c1). \n !r1,c1 t:Old(t,r1,c1)<-(NbAlive(t-1,r1,c1)=2|NbAlive(t-1,r1,c1)=3) & Young(t-1,r1,c1)&~Old(t-1,r1,c1).", True)#idp.Define("!r1,c1 t:Alive(t,r1,c1)<-NbAlive(t-1,r1,c1)=3.", True)#these cells can be dead
    #-------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
    #idp.Constraint("!r1[Row],r2[Row] c1[Column]:Young(1,r1,c1)<=Young(1,r2,c1)&r2="+str(realsize)+"-r1+1",True)#Mirror image rows
    #idp.Constraint("!c1[Column],c2[Column],r1[Row],r2[Row]:Young(1,r1,c1)<=Young(1,r2,c2)&c2=r1 &c1=r2",True)#mirror image diagonal

    #High density constraint 
    if (liv> size*size/2 and size >9):
            idp.Constraint("!r1[Row],c1[Column]:~(r1=10&c1=2)&~(r1=2&c1=10)&~(r1=2&c1=2)&~(r1=10&c1=10)&~Young(1,r1,c1) & ~EdgeCell(r1,c1)=>9>NbAlive(1,r1,c1)>3", True)#this is for the highest density still life
    
    #Decide to look for oscillators or spaceships
    if(genr>=2):
        idp.Constraint("?r1 c1 :~Young("+str(ceil((genr-1)/2))+",r1,c1)<=>Seed(r1,c1).", True)
        if(movH>0 or movV>0):
            idp.Constraint("!r1 c1: Seed(r1,c1)<=>Young("+str(genr-1)+",r1+"+str(movH)+",c1+"+str(movV)+").", True)#find glider
        idp.Constraint("!r1 c1 :(Young("+str(genr-1)+",r1,c1)|Old("+str(genr-1)+",r1,c1))<=>Seed(r1,c1).", True)

    else:
        idp.Constraint("!r1 c1 :(Young(1,r1,c1)|Old(1,r1,c1))<=>Seed(r1,c1).", True)

    #Keep track of the number of dead cells
    idp.Constraint("dead="+str(size*size)+"-#{ r1[Row] c1[Column] : Young(1,r1,c1) }", True) 
    # Add at least one Old cell in the second generation 
    idp.Constraint("#{r1[Row] c1[Column] : Old(1,r1,c1) }>=1.", True) 
    idp.check_sat()
    sols=idp.model_expand()
    #minimize the number of dead cells = Max density 
    #sols = idp.minimize("dead")

    for index, sol in enumerate(sols):
        if sol['satisfiable']:
            Gen= (sol['Young'])
            Old=(sol['Old'])
            print(type(Gen))
            print(size*size-sol['dead'][0])
            living=size*size-int(sol['dead'][0])
            return Gen,realsize, genr,Old
        else:
            print('unsat')
        




        
