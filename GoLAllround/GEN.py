from os import replace
from pyidp3.typedIDP import IDP
import ast
from math import ceil
from GridMaker import Moore
def Gens(size,liv,genr,movH,movV,symmetries):
    genr+=1
    realsize=size+2
    cells=size*size
    GRID= Moore(realsize,realsize)
    EdgeList=GRID[0]
    NbList=GRID[1]    

    idp = IDP("/home/jorne/Documents/idp/idp3-3.7.1-Linux/usr/local/bin/idp")
    idp.nbmodels = 1

    sliv=str(liv-1)

    idp.Type("Number", (0, 1000))

    idp.Type("Row", (1,realsize))
    idp.Type("Column", (1,realsize))


    idp.Type("NbA", list(range(0,9)))
    idp.Type("t", (0,genr-1))
    idp.Constant("dead: Number")


    #GoL is een use case van idp, 

    idp.Predicate("Nb(Row,Column,Row,Column)",NbList)
    idp.Predicate("EdgeCell(Row,Column)",EdgeList)
    idp.Predicate("Alive(t,Row,Column)")
    idp.Predicate("Seed(Row,Column)")
    idp.Function("NbAlive(t,Row, Column): NbA")

    #The edgecell massacre
    idp.Constraint("!r1 c1 g1:EdgeCell(r1,c1)=>~Alive(g1,r1,c1).", True)

    #How many neigbours are alive?
    idp.Constraint("!r1 c1 g1: NbAlive(g1,r1,c1)=#{r c:Alive(g1,r,c)&Nb(r1,c1,r,c)}.", True)
    idp.Constraint("#{r1[Row] c1[Column] : Seed(r1,c1) }>"+sliv, True) 

    
    #Rules of life -----------------------------------------------------------------------------------
    idp.Define("!r1,c1:Alive(0,r1,c1)<-Seed(r1,c1). \n !r1,c1 t:Alive(t,r1,c1)<-NbAlive(t-1,r1,c1)=3 & ~Alive(t-1,r1,c1). \n !r1,c1 t:Alive(t,r1,c1)<-(NbAlive(t-1,r1,c1)=2|NbAlive(t-1,r1,c1)=3) & Alive(t-1,r1,c1).", True)#idp.Define("!r1,c1 t:Alive(t,r1,c1)<-NbAlive(t-1,r1,c1)=3.", True)#these cells can be dead
    #-------------------------------------------------------------------------------------------------------
    Names=["C4","Horziontal","Vertical","Diagonal1","Diagonal2"]

    if(symmetries["C4"] and symmetries['C4'].isChecked()):
        idp.Constraint("!c1[Column],c2[Column],r1[Row],r2[Row]:Alive(1,r1,c1)<=Alive(1,r2,c2)&c2="+str(realsize+1)+"-r1 &c1=r2",True)#c4
    if(symmetries["Diagonal1"] and symmetries['Diagonal1'].isChecked()):
        idp.Constraint("!c1[Column],c2[Column],r1[Row],r2[Row]:Alive(1,r1,c1)<=Alive(1,r2,c2)&c2="+str(realsize+1)+"-r1 &c1="+str(realsize+1)+"-r2",True)#mirror image diagonal1
    if(symmetries["Diagonal2"] and symmetries['Diagonal2'].isChecked()):
        idp.Constraint("!c1[Column],c2[Column],r1[Row],r2[Row]:Alive(1,r1,c1)<=Alive(1,r2,c2)&c2=r1 &c1=r2",True)#mirror image diagonal2
   
    if(symmetries["Horziontal"] and symmetries['Horziontal'].isChecked()):
        idp.Constraint("!c1[Column],c2[Column],r1[Row],r2[Row]:Alive(1,r1,c1)<=Alive(1,r2,c2)&r2="+str(realsize+1)+"-r1 &c1=c2",True)#Horizontal
    if(symmetries["Vertical"] and symmetries['Vertical'].isChecked()):
        idp.Constraint("!c1[Column],c2[Column],r1[Row],r2[Row]:Alive(1,r1,c1)<=Alive(1,r2,c2)&c2="+str(realsize+1)+"-c1 &r1=r2",True)#vertical


    #idp.Constraint("!c1[Column],c2[Column],r1[Row],r2[Row]:Alive(1,r1,c1)<=Alive(1,r2,c2)&c2=r1 &c1=r2",True)#mirror image diagonal
    #idp.Constraint("!c1[Column],c2[Column],r1[Row],r2[Row]:Alive(1,r1,c1)<=Alive(1,r2,c2)&c2="+str(size)+"-c1 &r1="+str(size)+"-r2",True)#mirror image diagonal

    if (liv> size*size/2 ):
        if (size >5):
            idp.Constraint("!r1[Row],c1[Column]:~(r1="+str(size)+"&c1=2)&~(r1=2&c1="+str(size)+")&~(r1=2&c1=2)&~(r1="+str(size)+"&c1="+str(size)+")&~Alive(1,r1,c1) & ~EdgeCell(r1,c1)=>9>NbAlive(1,r1,c1)>3", True)#this is for the highest density still life
            #idp.Constraint("!r1[Row],c1[Column]:~Alive(1,r1,c1) & ~EdgeCell(r1,c1)=>9>NbAlive(1,r1,c1)>3", True)#this is for the highest density still life
        #pass
    

    if(genr>2):
        idp.Constraint("?r1 c1 :~Alive("+str(ceil((genr-1)/2))+",r1,c1)<=>Seed(r1,c1).", True)
        if(movH>0 or movV>0):
            idp.Constraint("!r1 c1: Seed(r1,c1)<=>Alive("+str(genr-1)+",r1+"+str(movH)+",c1+"+str(movV)+").", True)#find glider
        else:
            idp.Constraint("!r1 c1 :Alive("+str(genr-1)+",r1,c1)<=>Seed(r1,c1).", True)
    else:
        idp.Constraint("!r1 c1 :Alive("+str(genr-1)+",r1,c1)<=>Seed(r1,c1).", True)

    idp.Constraint("dead="+str(size*size)+"-#{ r1[Row] c1[Column] : Alive(1,r1,c1) }", True) 
    #idp.Constraint("#{t[t],r1,c1:(NbAlive(t-1,r1,c1)=2|NbAlive(t-1,r1,c1)=3) & Alive(t-1,r1,c1)}=0.", True) #statorless oscilator p=2

    idp.check_sat()
    sols=idp.model_expand()
    #sols = idp.minimize("dead")


    for index, sol in enumerate(sols):
        if sol['satisfiable']:
            Gen= (sol['Alive'])
            #Old=(sol['Old'])
            print(type(Gen))
            print(size*size-sol['dead'][0])
            living=size*size-int(sol['dead'][0])
            return Gen,realsize, genr
        else:
            print('unsat')
        




        
