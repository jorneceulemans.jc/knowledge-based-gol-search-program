from time import time
from typing import Dict
from PySide2.QtCore import QSize, Qt, QRect
from PySide2.QtWidgets import *
from PySide2.QtGui import QPalette, QColor,QFont
from GEN import Gens
from time import perf_counter


class Color(QWidget): # Ergens 'of the shelf' schrijven om idp te beschrijven

    def __init__(self, color):
        super(Color, self).__init__()
        self.setAutoFillBackground(True)
        #self.setStyleSheet("background-color: yellow;")

        palette = self.palette()
        palette.setColor(QPalette.Window, QColor(color))
        self.setPalette(palette)

class MainWindow(QMainWindow):
    def __init__(self,Gen):
        super(MainWindow, self).__init__()
        self.setWindowTitle("GoL visualiser")
        self.setMinimumSize(400, 800)
        palette = self.palette()
        palette.setColor(QPalette.Window, QColor(255, 255, 255))
        self.setPalette(palette)
        self.setAutoFillBackground(True)
        
        self.Gen=Gen[0]
        self.rsize=Gen[1]
        #self.old=Gen[3]
        self.genr=1
        self.mainlay = QVBoxLayout()
        self.layer1 = QHBoxLayout()  
        self.layer2 = QVBoxLayout()  
        self.layer3 = QVBoxLayout()   
        self.layer4 = QVBoxLayout()   
  
        RadioButtons=QHBoxLayout()
        #sliders
        self.Sbox=[]
        self.CreateBox([3,60])#0
        self.CreateBox([0,1000])#1
        self.CreateBox([1,20])#2
        self.CreateBox([0,10])#3
        self.CreateBox([0,10])#4
        #labels
        self.labels=[]
        self.CreateLabel('Size of the inner grid:')#0
        self.CreateLabel('Minimum amount of lives:')#1
        self.CreateLabel('Period:')#2
        self.CreateLabel('Symmetry:')#3
        self.CreateLabel('Movement vertical:')#4
        self.CreateLabel('Movement horizontal:')#5
        self.CreateLabel(timer)#6
        #buttons
        button=QPushButton("Let's Go",self)
        button.resize(10,10)
        button.setStyleSheet("background-color: red")
        button.move(50, 50)        
        #self.button.setCheckable(True)
        button.clicked.connect(self.the_button_was_clicked)

        self.button=QPushButton("Let's Go")
        
        self.layer2.addWidget(self.labels[0])#size of the grid
        self.layer2.addWidget(self.Sbox[0])
        self.layer2.addWidget(self.labels[1])#min number of lives
        self.layer2.addWidget(self.Sbox[1])
        self.layer2.addWidget(self.labels[2])#Period
        self.layer2.addWidget(self.Sbox[2])
        self.layer2.addWidget(self.labels[4])#Move horizontal
        self.layer2.addWidget(self.Sbox[3])
        self.layer2.addWidget(self.labels[5])#move vertical
        self.layer2.addWidget(self.Sbox[4])
        self.layer3.addWidget(self.labels[3])

        Names=["C4","Horziontal","Vertical","Diagonal1","Diagonal2"]
        self.RBs={}

        for i in range(5):
            radBut=QRadioButton(Names[i])
            self.RBs[Names[i]]=radBut
            self.RBs[Names[i]].setCheckable(True)
            self.layer3.addWidget(self.RBs[Names[i]])
            
            
        self.layer3.addWidget(self.labels[6])
        self.layer3.addWidget(button)
        
        self.layers=[]
        self.widgets=[]
        #grid
        self.oldgen=1
        self.getGrid()
        widget2=QWidget()
        widget3=QWidget()
        widget4=QWidget()
        widget2.setLayout(self.layer2)
        widget3.setLayout(self.layer3)
        widget4.setLayout(self.layer4)
        widget4.setFixedWidth(350)

        self.layer1.addWidget(widget2)
        self.layer1.addWidget(widget3)
        self.layer1.addWidget(widget4)
        self.widget = QWidget()
        self.widget.setLayout(self.layer1)
        self.setCentralWidget(self.widget)

    def CreateBox(self,range):
        Sbox=QSpinBox()
        Sbox.setRange(range[0],range[1])
        self.Sbox.append(Sbox)

    def CreateLabel(self,text):
        label=QLabel(text)
        label.setFixedHeight(50)
        label.setAlignment(Qt.AlignCenter)
        self.labels.append(label)
    def getGrid(self):
        for generation in range(self.genr):
            self.layers.append(QGridLayout())
            for lenght in range(self.rsize):
                for width in range(self.rsize):
                    self.layers[generation].addWidget(Color('light gray'),width,lenght)
            
            for cell in self.Gen:
                if cell[0]==generation:
                    self.layers[generation].addWidget(Color('black'),cell[1]-1,cell[2]-1)
            # for granny in self.old:
            #     if granny[0]==generation:
            #         self.layers[generation].addWidget(Color('dark blue'),granny[1]-1,granny[2]-1)

            self.layers[generation].setHorizontalSpacing(1)
            self.layers[generation].setVerticalSpacing(1)
            self.widgets.append(QWidget())
            self.widgets[generation].setLayout(self.layers[generation])
            self.widgets[generation].setFixedWidth(300-self.genr*30)
            self.widgets[generation].setFixedHeight(300-self.genr*30)
            self.layer4.addWidget(self.widgets[generation])

            #self.mainlay.addWidget(self.widgets[generation])
    
         
    def the_button_was_clicked(self):
        
        t1=perf_counter()
        Gen=Gens(self.Sbox[0].value(),self.Sbox[1].value(),self.Sbox[2].value(),self.Sbox[3].value(),self.Sbox[4].value(),self.RBs)
        t2=perf_counter()
        t3=str(round(t2-t1,3))

        if (type(Gen[0])==type(self.Gen)):

            self.Gen=Gen[0]
            self.rsize=Gen[1]
            self.genr=Gen[2]
            #self.old=Gen[3]             
            timer='found after '+ str(t3)+ ' seconds'
            self.labels[6].setText(timer)
            
            for z in range(self.oldgen):#delete old grids
                for i in reversed(range(self.layers[z].count())): 
                    self.layers[z].itemAt(i).widget().setParent(None)
                
            for i in (range(0,self.oldgen)):
                self.mainlay.removeWidget(self.widgets[i])
            self.getGrid()
            self.oldgen=self.genr
        else:
            timer='Nothing found after '+ str(t3)+ ' seconds'
            self.labels[6].setText(timer)
       
        self.mainlay.update()
        self.widget.update()
        self.update()

def CreateWindow(Gen):

    app = QApplication([])
    window= MainWindow(Gen)
    window.show()
    app.exec_()
if __name__ == "__main__":
    Names=["C4","Horziontal","Vertical","Diagonal1","Diagonal2"]
    RBs={}
    for i in range(5):
        RBs[Names[i]]=None
    t1=perf_counter()
    Gen=Gens(8,0,2,0,0,RBs)#size,liv,genr,movH,movV

    t2=perf_counter()
    t3=str(round(t2-t1,3))
    timer='found after '+ str(t3)+ ' seconds'

    CreateWindow(Gen)